package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class DashboardPage extends ProjectMethods {
	
	
	public DashboardPage() {
		
		
		PageFactory.initElements(driver, this);
	}
	
	
	public VendorSearchPage MouseOverVendor() {
		
		
		 Actions builder = new Actions(driver);
		   
		  
		  WebElement vendor = driver.findElementByXPath("(//button[text()=' Vendors'])");
		  
		 // WebElement search = driver.findElementByPartialLinkText("Search for Vendor");
		  
		  
		  builder.moveToElement(vendor).perform();
		  builder.moveToElement(driver.findElementByLinkText("Search for Vendor")).click().perform();
		  
		  
		  return new VendorSearchPage();
	}
	
	

}
