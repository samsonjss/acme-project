package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import com.framework.design.ProjectMethods;


public class loginpages extends ProjectMethods{
	
	
	public loginpages() {
		//apply PageFactory
		PageFactory.initElements(driver, this); 
	}
	
	@FindBy(how = How.ID,using="username") WebElement email;
	@FindBy(how = How.ID,using="password") WebElement password;
	@FindBy(how = How.ID,using="buttonLogin") WebElement buttonLogin;

	public loginpages enterUsername(String data) {
		
		driver.findElementById("email").clear();
		  
		driver.findElementById("email").sendKeys(data);
		
		return this;
	}
	
	
	public loginpages enterPassword(String data) {
		
		 driver.findElementById("password").clear();
		  
		 driver.findElementById("password").sendKeys(data);

		 return this;
			
		
	}
	
	
	public DashboardPage clickLogin() {
		
		driver.findElementById("buttonLogin").click();
		return new DashboardPage();
	}
	
}
