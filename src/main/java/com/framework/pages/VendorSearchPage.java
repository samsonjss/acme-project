package com.framework.pages;

import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class VendorSearchPage extends ProjectMethods{
	
	
	
	public VendorSearchPage() {
		
		PageFactory.initElements(driver, this);
		
	
	}
	
	
	public VendorSearchPage vendTaxid(String data) {
		
		
		 driver.findElementById("vendorTaxID").sendKeys(data);
		 
		 return this;
		
	}
	
	
	
	public vendorSearchResultPage clickSearch() {
		
		
		 driver.findElementById("buttonSearch").click();
		 
		 
		 return new vendorSearchResultPage();
	}

}
