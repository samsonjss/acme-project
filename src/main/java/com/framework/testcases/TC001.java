package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.loginpages;

public class TC001 extends ProjectMethods {
	
	@BeforeTest
	public void setdata() {
		
		/*LoginPage lp = new LoginPage();
		lp.enterUsername("");
		lp.enterPassword("");
		lp.clickLogin();*/
		
		testCaseName ="TC001LoginLogout";
		
		testDescription="Login to ACME";
		
		testNodes="LoginACME";
		
		author="SAMSON";
		
		category="smoke";
		
		dataSheetName="TC_001";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String username, String password, String taxid) {
		
		new loginpages()
		.enterUsername(username)
		.enterPassword(password)
		.clickLogin()
		.MouseOverVendor()
		.vendTaxid(taxid)
		.clickSearch();
		
		
	}

	
	
	

}
